const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookSchema = new Schema ({
    feTitle: {type: String, required: true},
    feQuotation: String,
    feAuthor: String,
    feNbPages: {type:Number, min: 1},
    fePublication: {type:Number, max: 9999},
    feType: String,
    feSynopsis: String,
    feImage: String,
    feCreateAt: {type: Date, default: Date.now}
});

bookSchema.pre("save", next => {
    now = new Date();
    if(!this.createAt) {
        this.createAt = now;
    };
    next();
});


const Book = mongoose.model('Book', bookSchema)

module.exports = Book;