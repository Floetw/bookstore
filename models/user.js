const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now
    }
});

UserSchema.pre("save", next => {
    now = new Date();
    if(!this.createAt) {
        this.createAt = now;
    };
    next();
});

const User = mongoose.model('User', UserSchema);

module.exports = User;