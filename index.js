const express = require('express');
const app = express(); // exécute express
const mongoose = require('mongoose'); // on appelle mongoose
// const config = require('config');
const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');

// PASSPORT CONFIG
require('./config/passport')(passport);

// DB CONFIG
const db = require('./config/keys').mongoURI;

// CONNECT TO MONGODB
mongoose
    .connect(
        db,
        {useNewUrlParser:true}
    )
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

// // CONFIG MONGOOSE (ADAPTATEUR DE LECTURE DE LA BDD)
// mongoose.connect(config.DBHost, { useNewUrlParser: true }); // chemin de la base de données
// // const db = mongoose.connection; // connexion à la BDD
// // db.on('error', console.error.bind(console, 'connection error:'));
// // db.once('open',function(){
// //     console.log('[MongoDB is connected !]');
// });

// CONFIG DOSSIER STATIQUE
app.use(express.static(__dirname + '/public'));

// EXPRESS BODY PARSER
app.use(express.urlencoded({extended: false})); // body parser est un analyseur de body, permet de récupérer les paramètres / il est intégré dans la dernière version d'express

// USE PUG
app.set('view engine', 'pug');

// EXPRESS SESSION
app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
);
    
// PASSPORT MIDDLEWARE
app.use(passport.initialize());
app.use(passport.session());
    
// CONNECT FLASH
app.use(flash());

// MES ROUTEURS
const booksRouter = require('./routes/booksRoutes');
const newBookRouter = require('./routes/newBookRouter');
const userRouter = require('./routes/userRouter');

// MES ROUTES
app.get('/', function(req, res){
    res.render('home')
})
app.get('/contact', function(req, res){
    res.render('contact')
})
app.use('/book', booksRouter);
app.use('/user', userRouter);
app.use('/', newBookRouter);



// listen the server
const PORT = process.env.PORT || 5000; // quand c'est en ligne, cela doit générer un port, sinon en local c'est le port 5000
app.listen(PORT, console.log(`Server started on port ${PORT}`));