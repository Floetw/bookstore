const booksList = [
    {title:'Harry Potter à l école des sorcier', author:'J.K Rowling', pagesNb:'456 p', publication:'10 août 2001', type:'fantastic', synopsis:'blabla'},
    {title:'2', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'3', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'4', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'', author:'', pagesNb:'', publication:'', type:'', synopsis:''},
    {title:'', author:'', pagesNb:'', publication:'', type:'', synopsis:''}
]

// on exporte la liste des livres pour qu'elle soit appelée par le routeur booksRoutes.js
module.exports = booksList;