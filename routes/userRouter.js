const express = require('express');
const userRouter = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

// LOAD USER MODEL
const User = require('../models/User');
const { forwardAuthenticated, ensureAuthenticated } = require('../config/auth');

//SHOW DASHBOARD PAGE
userRouter.get('/dashboard', ensureAuthenticated, (req, res,) => {
    res.render('dashboard', {
        user : req.user
    })
});

// SHOW LOGIN PAGE
userRouter.get('/login', forwardAuthenticated, (req, res) => {
    req.session.connected = true;
    res.render('login');
});

// SHOW REGISTER PAGE
userRouter.get('/register', forwardAuthenticated, (req, res) => res.render('register'));

// POST LOGIN PAGE
userRouter.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/user/dashboard',
        failureRedirect: '/user/login',
        failureFlash: true
    })(req, res, next);
});

// POST REGISTER PAGE
// let sess = '';
userRouter.post('/register', (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];
    
    if (!name || !email || !password || !password2) {
        errors.push({ msg: 'Please enter all fields' });
    }

    if (password != password2) {
        errors.push({ msg: 'Passwords do not match' });
    }

    if (password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters' });
    }

    if (errors.length > 0) {
        res.render('register', {
            errors,
            name,
            email,
            password,
            password2
        });
    } else {
        User.findOne({ email: email }).then(user => {
            if (user) {
                errors.push({ msg: 'Email already exists' });
                res.render('register', {
                    errors,
                    name,
                    email,
                    password,
                    password2
                });
            } else {
                const newUser = new User(
                    {
                        name,
                        email,
                        password
                    }
                );
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        console.log(newUser);
                        newUser
                            .save()
                            .then(user => {
                                res.redirect('/user/login');
                                req.flash(
                                    'success_msg',
                                    'You are now registered and can log in'
                                );
                            })
                            .catch(err => console.log(err));
                    });
                });
            }
        })
    }
})

// LOG OUT
userRouter.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/');
});


module.exports = userRouter;
