const express = require('express');
const newBookRouter = express.Router();
const Book = require('../models/bookModel'); //pour avoir accès aux livres de la base de données

// accéder à la vue
newBookRouter.get('/new', function(req, res){
    res.render('newBook')
})

//Pour créer un livre
// On envoie les datas saisies dans le formulaire pour ajouter un nouveau livre
newBookRouter.post('/new', function(req,res){
    const newBook = new Book(req.body); // on vérifie l'intégrité des données grâce au schéma créé dans le fichier bookModel.js / les infos sont récupérées dans le body (form) de la page
    newBook.save((function(err, bookDB){
        if(err) res.send(err) // on vérifie s'il y a des erreurs
        res.render('bookSaved');
    }))
});


module.exports = newBookRouter;