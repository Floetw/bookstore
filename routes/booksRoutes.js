const express = require('express');
const booksRouter = express.Router();
// const booksList = require ('../booksList');
const Book = require('../models/bookModel'); //pour avoir accès aux livres de la base de données

// pour voir la liste des livres : GET
booksRouter.get('/all', function(req, res){
    Book.find({}, function(err, booksDB){ // méthode qui appartient à Mongoose
        if(err) res.send(err);
        res.render('booksList', {books:booksDB});
    })
    
})

// pour accéder au formulaire de modification des infos d'un livre : GET
booksRouter.get('/update/:id', function(req, res){
    Book.find({_id:req.params.id},function(err, bookDB){
        if(err) res.send(err);
        res.render('updatedBook', {book:bookDB[0]});
    })
});

// pour modifier les infos d'un livre : POST
booksRouter.post('/update/:id', function(req, res){
    Book.updateOne({_id:req.params.id}, req.body, function(err, bookDB){
        if(err) res.send(err);
        res.render('bookSaved');
    })
});

// pour supprimer un livre : DELETE
booksRouter.get('/delete/:id', function(req, res){
    Book.deleteOne({_id:req.params.id}, function(err, bookDB){
        if(err) res.send(err);
        res.render('deletedBook');
    })
});

// pour voir un livre : GET
booksRouter.get('/:id', function(req, res){
    // const book = Book[req.params.id -1];
    // res.render('book', {book:book}) // 1 : pug, 2 : ancre pour le fichier pug, 3 : .js ou collection dans MongoDB
    Book.find({_id:req.params.id}, function(err, bookDB){
        if(err) res.send(err);
        res.render('book', {book:bookDB[0]});
    })
});





// on exporte le router des livres pour qu'il soit utilisé dans index.js
module.exports = booksRouter;