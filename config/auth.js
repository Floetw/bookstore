module.exports = {
    ensureAuthenticated: function(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        req.flash('error_msg', 'Please log in to view that resource');
        res.redirect('/user/login');
        },
    forwardAuthenticated: function(req, res, next) {
        // si je ne suis pas authentifié, je suis redirigé
        if (!req.isAuthenticated()) {
            return next();
        }
        // si je suis authentifié je vais sur l'url /dashboard
        res.redirect('/user/dashboard');      
    }
};